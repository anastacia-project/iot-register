# # -*- coding: utf-8 -*-
# settings.py
"""
This python module contains the main url and static values for the PoC of IoT Register inside the ANASTACIA European Project.

"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

import logging, os
# Logging 
from logger.handlers import KafkaHandler
import logging
logging.basicConfig(level=logging.INFO)

# remote logging
remote_logger = logging.getLogger("remote_logger")
remote_logger.setLevel(logging.INFO)
remote_logger.propagate = False

# Set remote logging handler
#REMOTE_LOGGING_URL = ['172.17.0.1:9092']
REMOTE_LOGGING_URL = ["{}:{}".format(os.environ["RL_ADDR"],os.environ["RL_PORT"])]
TOPIC = 'anastacia-log'
try:
	kh = KafkaHandler(REMOTE_LOGGING_URL, TOPIC)
	remote_logger.addHandler(kh)
except Exception as e:
	logging.info(e)

MUD_LOADER = "MSPLMUDLOADER"
POLICY_TEMPLATES_URL = "http://{}:{}/api/v1/mspl-templates/?capabilities=".format(os.environ["PR_ADDR"],os.environ["PR_PORT"])
#SECURITY_ORCHESTRATOR_API = "http://{}:{}/meservice/".format(os.environ["SO_ADDR"],os.environ["SO_PORT"])
SECURITY_ORCHESTRATOR_API = "http://{}:{}/enforce/".format(os.environ["SO_ADDR"],os.environ["SO_PORT"])
NOTIFY_TO = os.environ["NOTIFY_TO"]
ORCHESTRATION = "Orchestration"
TRAFFIC_DIVERT = "Traffic_Divert"
#SYSTEM_MODEL_URL = "http://{}:{}/api/swagger/".format(os.environ["SM_ADDR"],os.environ["SM_PORT"])
SM_USER = os.environ["SM_USER"]
SM_PASS = os.environ["SM_PASS"]

# For MUD translation example
SYS_MODEL = {"http://iot_controller.com": "2001:720:1710:4:5054:ff:feec:e209/128"}
