#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Utils for ANASTACIA micro-orchestrator
"""

__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2017, ANASTACIA Project"
__credits__ = ["Antonio Skarmeta, Jorge Bernal, Jordi Ortiz, Alejandro M. Zarca"]
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Testing"

import requests
import time
import datetime
import csv
import os.path


INLINE_SEPARATOR = "----------" 
TIMER = True

"""
class CSVWriter:

	file = None

	@staticmethod
	def init_csv(filename):
		CSVWriter.file = open(filename,"a")

	@staticmethod
	def add_line_to_csv(row):
		CSVWriter.file.write(row+"\n")

"""


def print_title(title):
	print("\n"+INLINE_SEPARATOR+title+INLINE_SEPARATOR+"\n")


def timing(target):
	def timing(f):
		def wrap(*args):
			if not TIMER:
				return

			filename = "{}.csv".format(target)
			file_exists = os.path.isfile(filename)

			with open("{}.csv".format(target), 'a') as csvfile:
				fieldnames = ["{}_START_TIME".format(target),"{}_END_TIME".format(target),"{}_TOTAL_TIME".format(target)]
				csv_writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
				if not file_exists:
					csv_writer.writeheader()

				time1 = time.time()
				#starting_message = "%s STARTING TIME;%0.3fms" % (f.__name__, time1*1000)
				starting_message = "%s STARTING TIME;%s" % (f.__name__, datetime.datetime.fromtimestamp(time1).strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])
				print(starting_message)
				#logger.info(starting_message)
				#CSVWriter.add_line_to_csv(starting_message)
				ret = f(*args)
				time2 = time.time()
				ending_message = "%s ENDING TIME;%f" % (f.__name__, time2)
				print(ending_message)
				#logger.info(ending_message)
				#CSVWriter.add_line_to_csv(ending_message)

				# time in miliseconds
				total_time = (time2-time1)
				total_message = '%s FUNCTION TOOK;%0.3f;ms' % (f.__name__, (time2-time1)*1000.0)
				print(total_message)
				#logger.info(total_message)
				#CSVWriter.add_line_to_csv(total_message)
				csv_writer.writerow({fieldnames[0]:time1,fieldnames[1]:time2,fieldnames[2]:total_time})

			return ret
		return wrap
	return timing

def rest_api(url, method, headers, data, auth, timeout=None):
	response = getattr(requests,method)(url,headers=headers, data=data, auth=auth,verify=False, cert=('old/sc-cert.pem','old/sc-privkey.pem'),timeout=timeout)
	#response = requests.post(url,headers=headers, data=data, auth=auth)
	print("Response Code:" + str(response.status_code))
	assert response.status_code in [200,201,204], "Error Status Code: %r Response: %s" % (response.status_code,response.text)
	return response
