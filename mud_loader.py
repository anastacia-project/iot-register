#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
MUD Enforcer PoC implementation for ANASTACIA European Project.
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2017, ANASTACIA Project"
__credits__ = ["Antonio Skarmeta, Jorge Bernal, Jordi Ortiz, Alejandro M. Zarca"]
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "PoC"

from abc import ABCMeta, abstractmethod
from settings import SYS_MODEL, ORCHESTRATION, TRAFFIC_DIVERT, POLICY_TEMPLATES_URL, logging
logger = logging.getLogger(__name__)
import json
import requests

class MUDLOADER:
	__metaclass__ = ABCMeta

	def get_mud_file(self,mud_url):
		# TODO
		#mud_file = requests.get(mud_url)
		f = open("test/iot-controller-coap.json", "r")
		mud_file = f.read()
		return mud_file

	@abstractmethod
	def load_policies(self, mud_file):
		pass


class MSPLMUDLOADER(MUDLOADER):

	FORWARD = "forwarding"
	ACTION_CAPABILITIES = {FORWARD:TRAFFIC_DIVERT}

	def get_mspl_template(self, capability):

		try:
			response = requests.get("{}{}".format(POLICY_TEMPLATES_URL,capability))
			json_response = json.loads(response.text)
			logger.info(json_response)
		except Exception as e:
			logger.info(str(e))
			return None
		return json_response[0]["mspl"]


	def fill_mspl_traffic_divert(self, mspl_template, source, destination):
		return mspl_template.replace("<SourceAddress>","<SourceAddress>{}".format(source))\
			.replace("<DestinationAddress>","<DestinationAddress>{}".format(destination))#\
			#.replace('id=""','id="{}"'.format("mspl_{}".format(uuid.uuid4().hex)))

	def fill_mspl_template(self,omspl_template, capability, source, destination):
		# TODO: decide the capability depending on the actions
		mspl_template = self.get_mspl_template(capability)
		logger.info("mspl_template: {}".format(mspl_template))
		mspl = getattr(self,"fill_mspl_{}".format(capability.lower()))(mspl_template, source, destination)
		return omspl_template.replace("</ITResourceOrchestration>","{}</ITResourceOrchestration>".format(mspl))


	def generate_mspl_to(self,omspl_template, capability, source,destination):
		return self.fill_mspl_template(omspl_template, capability, source, destination)



	def generate_mspl_fr(self,omspl_template, capability, source,destination):
		return self.fill_mspl_template(omspl_template, capability, destination, source)


	def to_mspl(self,mud_file, iot_addr):
		mud_obj = json.loads(mud_file)

		# Get a policy for orchestration
		omspl_template = self.get_mspl_template(ORCHESTRATION)

		#from_device_policy = mud_obj["ietf-mud:mud"]["from-device-policy"]["access-lists"]["access-list"][0]["name"]
		#to_device_policy = mud_obj["ietf-mud:mud"]["to-device-policy"]["access-lists"]["access-list"][0]["name"]

		acls = mud_obj["ietf-access-control-list:acls"]["acl"]
		mspl_policies = []
		logger.info("ACLS:{}".format(acls))
		for acl in acls:
			from_to = acl["name"][-2:]
			logger.info("AAAAAAAAAAAAAAAAAAAA:{}".format(from_to))
			aces = acl["aces"]["ace"]
			for ace in aces:
				matches = ace["matches"]
				source = SYS_MODEL[matches["ietf-mud:mud"]["controller"]]
				destination = iot_addr
				actions = ace["actions"]
				capability = self.ACTION_CAPABILITIES[next(iter(actions))]
				logger.info("Calling generate_mspl_{}".format(from_to))
				omspl_template = getattr(self,"generate_mspl_{}".format(from_to))(omspl_template, capability, source, destination)
				#logger.info(omspl_template)


		logger.info(omspl_template)
		return omspl_template
		# Extract capability from mud_file

		# Extract source and destination

		# Get the MSPL template

		# Customize it

		# return mspl

	def load_policies(self, register_msg):
		#Reformat the ipv6
		for iot_if in register_msg["ifaces"]:
			ip_addr = iot_if["ip_addr"]
			iot_if["ip_addr"] = ":".join(ip_addr[i:i+4] for i in range(0,len(ip_addr),4))
		# Add the /128 for a ipv6 host
		iot_if["ip_addr"] = iot_if["ip_addr"]+"/128"
		mud_url = register_msg["mud-url"]
		mud_file = self.get_mud_file(mud_url)
		logger.info("MUD-FILE: {}".format(mud_file))
		omspl = self.to_mspl(mud_file, iot_if["ip_addr"])
		return omspl


"""
	def notify(self, json_message):
		# Retrieve a FWD template from the Policy repository and fill it with the received information.
		POLICY_REPOSITORY_SERVICE_URL = "http://policy-repository.es:8004/api/v1/mspl-templates/?capabilities=divert"
		#POLICY_REPOSITORY_SERVICE_URL = "http://10.79.7.30:8004/api/v1/mspl-templates/?capability=divert"

		source = json_message["ifaces"][0]["ip_addr"]
		targets = json_message["authorization_info"]

		try:
			response = requests.get(POLICY_REPOSITORY_SERVICE_URL)
			json_response = json.loads(response.text)
			logger.info(json_response)
		except Exception as e:
			logger.info(str(e))
			return
		
		# The Security orchestrator must allow the traffic according on the new IoT device capabilties
		mspl_fwd_template = json_response[0]["mspl"]
		logger.info(mspl_fwd_template)
		mspl_list = []
		for target in targets:
			target = target["de"]
			mspl_fwd_1 = mspl_fwd_template.replace("<SourceAddress>","<SourceAddress>{}".format(source))\
			.replace("<DestinationAddress>","<DestinationAddress>{}".format(target))\
			.replace('ID=""','ID="{}"'.format("mspl_{}".format(uuid.uuid4().hex)))
			mspl_list.append({"mspl":mspl_fwd_1})
			mspl_fwd_2 = mspl_fwd_template.replace("<SourceAddress>","<SourceAddress>{}".format(target))\
			.replace("<DestinationAddress>","<DestinationAddress>{}".format(source))\
			.replace('ID=""','ID="{}"'.format("mspl_{}".format(uuid.uuid4().hex)))
			mspl_list.append({"mspl":mspl_fwd_2})
		json_response = {"mspl_list":mspl_list}
		logger.info(json.dumps(json_response))
		super(SecurityOrchestratorIoTRegistrationNotifier,self).notify(json_response)


"""