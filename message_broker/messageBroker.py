#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Enabler API client.
"""


__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2017, ANASTACIA Project"
__credits__ = ["Antonio Skarmeta, Jorge Bernal, Jordi Ortiz, Alejandro M. Zarca"]
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Testing"

from abc import ABCMeta, abstractmethod
import pika


class MessageBroker:
	__metaclass__ = ABCMeta


	def __init__(self,**kwargs):
		self.server_address = kwargs["server_address"]
		self.queue = kwargs["queue"]

	@abstractmethod
	def send(self, message):
		pass

	@abstractmethod
	def subscribe(self, callback):
		pass

class RabbitMQ(MessageBroker):
	
	def send(self, message):
		print("Sending message to Server address:",self.server_address,"queue:",self.queue)
		connection = pika.BlockingConnection(pika.ConnectionParameters(host=self.server_address))
		channel = connection.channel()

		channel.queue_declare(queue=self.queue)

		channel.basic_publish(exchange='',
		                      routing_key=self.queue,
		                      body=message)
		print(" [x] Sent ",message)
		connection.close()

	# The callback function must be defined as callback(ch, method, properties, body)
	def subscribe(self, callback):
		print("Subscribing to Server address:",self.server_address,"queue:",self.queue)
		connection = pika.BlockingConnection(pika.ConnectionParameters(host=self.server_address))
		channel = connection.channel()

		channel.queue_declare(queue=self.queue)

		#def callback(ch, method, properties, body):
		#    print(" [x] Received %r" % body)

		channel.basic_consume(self.queue,
								callback,
		                      auto_ack=True)

		print(' [*] Waiting for messages. To exit press CTRL+C')
		channel.start_consuming()
		

