#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Micro Orchestrator implementation for ANASTACIA PoC.
"""
from optparse import OptionParser
from messageBroker import RabbitMQ


__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2017, ANASTACIA Project"
__credits__ = ["Antonio Skarmeta, Jorge Bernal, Jordi Ortiz, Alejandro M. Zarca"]
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Testing"



def parse_options():
	# parse command line arguments
	parser = OptionParser(usage="Usage: %prog -s [server_address] -q [quque]",
	                    description="Example program sending rabbitmq messages.")
	parser.add_option("-s", action="store", type="string", dest="server_address", 
	                help="The IP Address of the rabbitmq server.")
	parser.add_option("-q", action="store", type="string", dest="queue",
	                help="The queue name.")
	parser.add_option("-m", action="store", type="string", dest="message",
	                help="The message.")


	(options, args) = parser.parse_args()

	return options

if __name__ == "__main__":

	options = parse_options()

	# This is the test for the subscribe to rabbitmq
	message_broker = RabbitMQ(server_address=options.server_address,queue=options.queue)
	message_broker.send(options.message)



"""

AUTH = ("admin", "admin")
HEADERS = {
    'Content-Type': 'application/yang.data+json',
}
METHOD = "post"

INLINE_SEPARATOR = "----------" 


ENABLERS = {"onos":{"security_control":"sdn","app_type":"json","auth":("karaf","karaf"),"operation":{
						"add_flow": {"method":"post","url": "http://192.168.57.1:8181/onos/v1/flows/of%3A000008002768a30a?appId=Dropper"},
						"delete_flow":{"method":"delete","url": "http://192.168.57.1:8181/onos/v1/flows/application/Dropper"}}},
			"odl":{"security_control":"sdn","app_type":"xml","auth":("admin","admin"),"operation":{
						"add_flow": {"method":"put","url":"http://172.17.0.2:8181/restconf/config/opendaylight-inventory:nodes/node/openflow:1/table/0/flow/Dropper"},
						"delete_flow":{"method":"delete","url": "http://192.168.57.1:8181/onos/v1/flows/application/Dropper"}}},
			"iptables":{"security_control":"iptables","app_type":"json","auth":("admin","admin"),"operation":{
						"add_flow": {"method":"put","url":"http://192.168.57.4:8000/iptables"},
						"delete_flow":{"method":"delete","url": "http://192.168.57.4:8000/iptables"}}}
			}

ENABLER = ENABLERS["iptables"]

def add_line_to_csv(row):
	file = open("output.csv","a")
	file.write(row+"\n")
	file.close()

def print_title(title):
	print("\n"+INLINE_SEPARATOR+title+INLINE_SEPARATOR+"\n")

def start_time():
	start = time.time()
	print("\n"+"STARTING TIME:"+str(start))
	return start

def end_time(start, task):
	end = time.time()
	elapsed_time = end - start
	print("\n"+"ENDING TIME:"+str(end))
	print("\n"+"ELAPSED TIME FOR "+task+" "+str(elapsed_time)+"s")
	add_line_to_csv(task+","+str(elapsed_time))
	return elapsed_time

def rest_api(url, method, headers, data, auth):
	response = getattr(requests,method)(url,headers=headers, data=data, auth=auth)
	#response = requests.post(url,headers=headers, data=data, auth=auth)
	print("Response Code:" + str(response.status_code))
	assert response.status_code in [200,201,204], "Error Status Code: %r" % response.status_code
	return response


def init_sensor_isolation():
	# --------------------Translate from HSPL to MSPL-------------------------------
	# curl -i -u "admin":"admin" -H "Content-Type: application/yang.data+json" -X POST http://localhost:8181/restconf/operations/h2mservice:h2mrefinement -d @h2mservice_input.json
	start = start_time()

	file = open("h2mservice_input_dev_isolation_ipv6.json","r") 
	h2mservice_input_json = file.read()

	print_title("Reading HSPl file:")
	print(h2mservice_input_json)

	print_title("Translating from HSPL to MSPL")
	HSPL_URL = "http://localhost:8181/restconf/operations/h2mservice:h2mrefinement"
	response = rest_api(HSPL_URL, METHOD, HEADERS, h2mservice_input_json, AUTH)

	# Get MSPL rules and composes the json

	json_data = json.loads(response.text) 

	mspl_rules = json_data["output"]["MSPL"]

	m2lservice_input_json = json.dumps({'input': {'mspl_rules': mspl_rules, 'security_control': ENABLER["security_control"]}})

	formatted_mspl_sules = '\n'.join(str(x) for x in mspl_rules)

	print(formatted_mspl_sules)
	end_hspl_mspl = end_time(start,"HSPL->MSPL")


	# ---------------------Translate from MSPL to Lower-------------------------------
	#curl -i -u "admin":"admin" -H "Content-Type: application/yang.data+json" -X POST http://localhost:8181/restconf/operations/m2lservice:m2ltranslate -d @m2lservice_input.json
	start = start_time()

	print_title("Translating from MSPL to Lower")
	MSPL_URL = "http://localhost:8181/restconf/operations/m2lservice:m2ltranslate"
	response = rest_api(MSPL_URL, METHOD, HEADERS, m2lservice_input_json, AUTH)

	#file = open("m2lervice_input_test.json","w")
	#file.write(m2lservice_input_json)
	#file.close()

	json_data = json.loads(response.text) 

	enabler_configuration = json_data["output"]["psa_config"]

	print(enabler_configuration)
	end_mspl_lower = end_time(start,"MSPL->Lower")



	# ---------------------Sending Configuration to OpenDaylight--------------------------------
	#curl -i -u "admin":"admin" -H "Content-Type: application/xml" -H "Accept: application/json" -X PUT http://192.168.57.1:8181/restconf/config/opendaylight-inventory:nodes/node/openflow:8796754191114/table/0/flow/Dropper -d @../odl_restconf_drop.conf
	start = start_time()

	print_title("Setting Configuration onto Enabler")

	operation = "add_flow"

	headers = {
	    'Content-Type': 'application/'+ENABLER["app_type"],
	}
	response = rest_api(ENABLER["operation"][operation]["url"], ENABLER["operation"][operation]["method"], headers, enabler_configuration, ENABLER["auth"])

	#file = open("m2lervice_input_test.json","w")
	#file.write(m2lservice_input_json)
	#file.close()

	print(response.text)
	end_lower_enabler = end_time(start,"Lower->Enabler")

	return end_hspl_mspl + end_mspl_lower +end_lower_enabler


def remove_sensor_isolation():

	operation = "delete_flow"
	headers = {
	    'Accept': 'application/json',
	}
	response = rest_api(ENABLER["operation"][operation]["url"], ENABLER["operation"][operation]["method"], headers, None, ENABLER["auth"])

	#file = open("m2lervice_input_test.json","w")
	#file.write(m2lservice_input_json)
	#file.close()

	print(response.text)

#init_sensor_isolation()


NUM_TEST = 1
acum = 0
try:
	os.remove("output.csv")
except IOError:
	print("output.csv does not exist")


for i in range(NUM_TEST):
	#remove_sensor_isolation()
	time.sleep(5)
	total_time = init_sensor_isolation()
	acum+=total_time
	print("TOTAL TIME: "+str(total_time))
	add_line_to_csv("TOTAL"+","+str(total_time))
	time.sleep(5)

average = acum/NUM_TEST
print("AVERAGE: "+str(average))
add_line_to_csv("AVERAGE"+","+str(average))

#remove_sensor_isolation()
"""