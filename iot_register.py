#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
IoT register service implementation for ANASTACIA European Project.
"""
import sys
from optparse import OptionParser
from message_broker.messageBroker import RabbitMQ
from iot_registration_notifier import *
import requests
import json
from settings import logging, remote_logger, MUD_LOADER, SECURITY_ORCHESTRATOR_API, NOTIFY_TO
logger = logging.getLogger(__name__)
from utils import timing
from datetime import datetime
import mud_loader as mud_loader_module
import time


__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2017, ANASTACIA Project"
__credits__ = ["Antonio Skarmeta, Jorge Bernal, Jordi Ortiz, Alejandro M. Zarca"]
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Testing"

#NOTIFY_TO = [IoTControllerIoTRegistrationNotifier,SecurityOrchestratorIoTRegistrationNotifier]


def parse_options():

	print(sys.argv)
	# parse command line arguments
	parser = OptionParser(usage="Usage: %prog -d [message_broker_addr] -q [queue_name]",
											description="Service client of queue for microOrchestrator.")
	parser.add_option("-d", action="store", type="string", dest="message_broker_addr", 
									help="Message broker server address e.g: 10.79.7.36.")
	parser.add_option("-q", action="store", type="string", dest="queue",
									help="Queue name, e.g. auth-queue")


	(options, args) = parser.parse_args()

	if not options.message_broker_addr: 
		parser.error("You must to provide a message broker address")
	if not options.queue:
		parser.error("You must to provide a message broker queue name")

	return options

"""
def notify(json_message,destinations=[]):
	for destination in destinations:
		try:
			response = requests.post(destination, json=json_message)
			logger.info(response)
		except Exception as e:
			logger.info(str(e))
"""

def load_default_policies(register_msg):
	# Verify if the device has some predefined policies and enforce them.
	# Get MUD file from the MUD url received by the IoT device.
	if "mud-url" in register_msg.keys():
		# create the mud loader class which in this case will be instntiated to mud mspl loader (see settings.py)
		mud_loader = getattr(mud_loader_module,MUD_LOADER)()
		return mud_loader.load_policies(register_msg)
	# Otherwise system apply the policies by default
	# TODO: build non-mud by default policies
	return None

def enforce_default_policies(default_policies):
	# Enforce the default policies
	try:
		response = requests.post(SECURITY_ORCHESTRATOR_API, data=default_policies)
		logger.info(response)
	except Exception as e:
		logger.info(str(e))


#@timing("IoTRegistration")
def auth_callback(ch, method, properties, body):
	try:
		logger.info("Received: {}".format(body))
		register_msg = json.loads(body)
		logging_message = {
		 "timestamp": datetime.timestamp(datetime.now()),
		 "from_module": "PANA_Bootstrapping",
		 "from_component": "PANA_Bootstrapping",
		 "to_module": "IOT_CONTROLLER",
		 "to_component": "IOT_REGISTER",
		 "incoming": True,
		 "method": "RabbitMQ",
		 "data": register_msg,
		 "notes": "IoT device Registration"
		}
		remote_logger.info(logging_message)
		logger.info(register_msg)
		# Notify IoT registration message
		for nofifier_class_name in NOTIFY_TO.split(","):
			logger.info(nofifier_class_name)
			notifier_class = globals()[nofifier_class_name]
			logger.info("notifier class detected {}".format(notifier_class))
			notifier = notifier_class()	
			notifier.notify(register_msg)
	except Exception as e:
		logger.info("Registration Error:")
		logger.info(e)
	
	# Verify if there are default policies to enforce (MUD approach)
	#default_policies = load_default_policies(register_msg)
	#if default_policies:
	#	enforce_default_policies(default_policies)








	# Send the information to the destinations
	#for notifier_class in NOTIFY_TO:
	#	notifier = notifier_class()
	#	notifier.notify(json_message)


	# Send the information to the IoT Controller
	#notify(json_message, [IOT_CONTROLLER_API,SECURITY_ORCHESTRATOR_API])
	"""
	try:
		response = requests.post(IOT_CONTROLLER_API, json=json_message)
		logger.info(response)
	except Exception as e:
		logger.info(str(e))

	try:
		response = requests.post(SECURITY_ORCHESTRATOR_API, json=json_message)
		logger.info(response)
	except Exception as e:
		logger.info(str(e))
	"""
	# Send the information to the Security Orchestrator
	#response = requests.post(SECURITY_ORCHESTRATOR_API, json=json_message)
	#print(response)

	#print(" [x] Received %r" % body)
	#IPV6_ADDR_STR_LEN = 32

	# Get the IPv6 & the PEMK
	#pac_ipv6_addr = body[:IPV6_ADDR_STR_LEN]
	#pemk = body[IPV6_ADDR_STR_LEN:]

	#print(" [x] PaC IPv6 %s" % pac_ipv6_addr)
	#print(" [x] PEMK %s" % pemk)

	# Send to IoT Controller the new IoT device information


if __name__ == "__main__":

	# This is the test for the subscribe to rabbitmq
	while True:
		try:
			options = parse_options()
			logger.info("queue is {}".format(options.queue))
			message_broker = RabbitMQ(server_address=options.message_broker_addr,queue=options.queue)
			logger.info("RabbitMQ connection success")
			message_broker.subscribe(auth_callback)
			
			# Test directly the IoT bootstrapping
			#fake_message = '{"id":"2002","name":"Sensor-2002","ifaces":[{"id":"1","net":"2001:720:1710:4/64","name":"6LoWPAN_1","mac_addr":"00:00:00:00:00:01","ip_addr":"20010720171000040000000000002002"}],"operatingSystem":{"name":"contiki","version":"odins_v1"},"software":[{"id":"1","name":"ErbiumServer","version":"3.14159"}],"model":"Wismote","location":{"latitude":"38.024548","longitude":"-1.172727","zone":"PEANA","description":"PEANA2"},"PEMK_key":"8a7e9d931d3d529588b5c0da5fa21425","authorization_info":[{"de":"2001:720:1710:4:5054:ff:feec:e209/128"},{"de":"2001:720:1710:4:5054:ff:febe:74b0/128"}],"resources":[{"resource":"/.well-known/core","description":"well-known","method":"GET","payload":"","port":"5683"},{"resource":"/60000/0/1","description":"LED_GET_PUT","method":"PUT","payload":"1","port":"5683"},{"resource":"/60000/0/2","description":"Temperatura_GET","method":"GET","payload":"","port":"5683"},{"resource":"/60000/0/3","description":"Humedad_GET","method":"GET","payload":"","port":"5683"},{"resource":"/60000/0/4","description":"PeriodicCounter","method":"GET","payload":"","port":"5683"},{"resource":"/60000/0/5","description":"POWER_OFF_PUT_1","method":"PUT","payload":"1","port":"5683"},{"resource":"/60000/0/6","description":"RESET_BOOT","method":"PUT","payload":"1","port":"5683"},{"resource":"/60000/0/7","description":"ACTIVATE_DTLS","method":"PUT","payload":"1","port":"5683"},{"resource":".60001","description":"TEMPERATURE","method":"GET","payload":"","port":"5683"}]}'
			#register_msg = json.loads(fake_message)
			#sec_orch_notif = SecurityOrchestratorIoTRegistrationNotifier()
			#sec_orch_notif.notify(register_msg)
		except Exception as e:
			logger.info("RabbitMQ connection error, Trying to reconnect...")
			logger.info(str(e))
			time.sleep(5)
			#print(str(e))


