# SDNSwitch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**type** | **str** |  | 
**state** | **str** |  | 
**name** | **str** |  | 
**tag** | **str** |  | 
**model** | **str** |  | 
**description** | **str** |  | 
**alias** | **str** |  | 
**softwares** | **list[int]** |  | [optional] 
**cloud** | **list[int]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


