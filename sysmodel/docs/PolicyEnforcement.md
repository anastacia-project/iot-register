# PolicyEnforcement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**mspl** | **str** |  | 
**type** | **str** |  | 
**status** | **str** |  | 
**create** | **datetime** |  | [optional] 
**update** | **datetime** |  | [optional] 
**device** | **int** |  | [optional] 
**sdnrule** | **int** |  | [optional] 
**capability** | **list[int]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


