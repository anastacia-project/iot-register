# swagger_client.CapabilityApi

All URIs are relative to *http://195.148.125.100:9000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**capability_create**](CapabilityApi.md#capability_create) | **POST** /capability/ | 
[**capability_delete**](CapabilityApi.md#capability_delete) | **DELETE** /capability/{id}/ | 
[**capability_list**](CapabilityApi.md#capability_list) | **GET** /capability/ | 
[**capability_partial_update**](CapabilityApi.md#capability_partial_update) | **PATCH** /capability/{id}/ | 
[**capability_read**](CapabilityApi.md#capability_read) | **GET** /capability/{id}/ | 
[**capability_update**](CapabilityApi.md#capability_update) | **PUT** /capability/{id}/ | 


# **capability_create**
> Capability capability_create(data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.CapabilityApi(swagger_client.ApiClient(configuration))
data = swagger_client.Capability() # Capability | 

try:
    api_response = api_instance.capability_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CapabilityApi->capability_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Capability**](Capability.md)|  | 

### Return type

[**Capability**](Capability.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **capability_delete**
> capability_delete(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.CapabilityApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique value identifying this capability.

try:
    api_instance.capability_delete(id)
except ApiException as e:
    print("Exception when calling CapabilityApi->capability_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique value identifying this capability. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **capability_list**
> InlineResponse2002 capability_list(page=page)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.CapabilityApi(swagger_client.ApiClient(configuration))
page = 56 # int | A page number within the paginated result set. (optional)

try:
    api_response = api_instance.capability_list(page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CapabilityApi->capability_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| A page number within the paginated result set. | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **capability_partial_update**
> Capability capability_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.CapabilityApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique value identifying this capability.
data = swagger_client.Capability() # Capability | 

try:
    api_response = api_instance.capability_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CapabilityApi->capability_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique value identifying this capability. | 
 **data** | [**Capability**](Capability.md)|  | 

### Return type

[**Capability**](Capability.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **capability_read**
> Capability capability_read(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.CapabilityApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique value identifying this capability.

try:
    api_response = api_instance.capability_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CapabilityApi->capability_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique value identifying this capability. | 

### Return type

[**Capability**](Capability.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **capability_update**
> Capability capability_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.CapabilityApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique value identifying this capability.
data = swagger_client.Capability() # Capability | 

try:
    api_response = api_instance.capability_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CapabilityApi->capability_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique value identifying this capability. | 
 **data** | [**Capability**](Capability.md)|  | 

### Return type

[**Capability**](Capability.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

