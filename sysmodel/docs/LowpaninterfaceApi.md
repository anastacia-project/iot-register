# swagger_client.LowpaninterfaceApi

All URIs are relative to *http://195.148.125.100:9000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**lowpaninterface_create**](LowpaninterfaceApi.md#lowpaninterface_create) | **POST** /lowpaninterface/ | 
[**lowpaninterface_delete**](LowpaninterfaceApi.md#lowpaninterface_delete) | **DELETE** /lowpaninterface/{id}/ | 
[**lowpaninterface_list**](LowpaninterfaceApi.md#lowpaninterface_list) | **GET** /lowpaninterface/ | 
[**lowpaninterface_partial_update**](LowpaninterfaceApi.md#lowpaninterface_partial_update) | **PATCH** /lowpaninterface/{id}/ | 
[**lowpaninterface_read**](LowpaninterfaceApi.md#lowpaninterface_read) | **GET** /lowpaninterface/{id}/ | 
[**lowpaninterface_update**](LowpaninterfaceApi.md#lowpaninterface_update) | **PUT** /lowpaninterface/{id}/ | 


# **lowpaninterface_create**
> LoWPANInterface lowpaninterface_create(data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.LowpaninterfaceApi(swagger_client.ApiClient(configuration))
data = swagger_client.LoWPANInterface() # LoWPANInterface | 

try:
    api_response = api_instance.lowpaninterface_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LowpaninterfaceApi->lowpaninterface_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**LoWPANInterface**](LoWPANInterface.md)|  | 

### Return type

[**LoWPANInterface**](LoWPANInterface.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **lowpaninterface_delete**
> lowpaninterface_delete(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.LowpaninterfaceApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this lo wpan interface.

try:
    api_instance.lowpaninterface_delete(id)
except ApiException as e:
    print("Exception when calling LowpaninterfaceApi->lowpaninterface_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this lo wpan interface. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **lowpaninterface_list**
> InlineResponse2009 lowpaninterface_list(page=page)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.LowpaninterfaceApi(swagger_client.ApiClient(configuration))
page = 56 # int | A page number within the paginated result set. (optional)

try:
    api_response = api_instance.lowpaninterface_list(page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LowpaninterfaceApi->lowpaninterface_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| A page number within the paginated result set. | [optional] 

### Return type

[**InlineResponse2009**](InlineResponse2009.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **lowpaninterface_partial_update**
> LoWPANInterface lowpaninterface_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.LowpaninterfaceApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this lo wpan interface.
data = swagger_client.LoWPANInterface() # LoWPANInterface | 

try:
    api_response = api_instance.lowpaninterface_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LowpaninterfaceApi->lowpaninterface_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this lo wpan interface. | 
 **data** | [**LoWPANInterface**](LoWPANInterface.md)|  | 

### Return type

[**LoWPANInterface**](LoWPANInterface.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **lowpaninterface_read**
> LoWPANInterface lowpaninterface_read(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.LowpaninterfaceApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this lo wpan interface.

try:
    api_response = api_instance.lowpaninterface_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LowpaninterfaceApi->lowpaninterface_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this lo wpan interface. | 

### Return type

[**LoWPANInterface**](LoWPANInterface.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **lowpaninterface_update**
> LoWPANInterface lowpaninterface_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.LowpaninterfaceApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this lo wpan interface.
data = swagger_client.LoWPANInterface() # LoWPANInterface | 

try:
    api_response = api_instance.lowpaninterface_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LowpaninterfaceApi->lowpaninterface_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this lo wpan interface. | 
 **data** | [**LoWPANInterface**](LoWPANInterface.md)|  | 

### Return type

[**LoWPANInterface**](LoWPANInterface.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

