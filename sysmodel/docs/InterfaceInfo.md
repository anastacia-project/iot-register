# InterfaceInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**mac** | **str** |  | 
**ipv4** | **str** |  | [optional] 
**ipv4_mask** | **str** |  | [optional] 
**local_ipv6** | **str** |  | [optional] 
**local_ipv6_mask** | **str** |  | [optional] 
**global_ipv6** | **str** |  | [optional] 
**global_ipv6_mask** | **str** |  | [optional] 
**global_prefix** | **str** |  | [optional] 
**interface** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


