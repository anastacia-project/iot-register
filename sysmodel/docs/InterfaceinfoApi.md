# swagger_client.InterfaceinfoApi

All URIs are relative to *http://195.148.125.100:9000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**interfaceinfo_create**](InterfaceinfoApi.md#interfaceinfo_create) | **POST** /interfaceinfo/ | 
[**interfaceinfo_delete**](InterfaceinfoApi.md#interfaceinfo_delete) | **DELETE** /interfaceinfo/{id}/ | 
[**interfaceinfo_list**](InterfaceinfoApi.md#interfaceinfo_list) | **GET** /interfaceinfo/ | 
[**interfaceinfo_partial_update**](InterfaceinfoApi.md#interfaceinfo_partial_update) | **PATCH** /interfaceinfo/{id}/ | 
[**interfaceinfo_read**](InterfaceinfoApi.md#interfaceinfo_read) | **GET** /interfaceinfo/{id}/ | 
[**interfaceinfo_update**](InterfaceinfoApi.md#interfaceinfo_update) | **PUT** /interfaceinfo/{id}/ | 


# **interfaceinfo_create**
> InterfaceInfo interfaceinfo_create(data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.InterfaceinfoApi(swagger_client.ApiClient(configuration))
data = swagger_client.InterfaceInfo() # InterfaceInfo | 

try:
    api_response = api_instance.interfaceinfo_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling InterfaceinfoApi->interfaceinfo_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**InterfaceInfo**](InterfaceInfo.md)|  | 

### Return type

[**InterfaceInfo**](InterfaceInfo.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **interfaceinfo_delete**
> interfaceinfo_delete(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.InterfaceinfoApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this interface info.

try:
    api_instance.interfaceinfo_delete(id)
except ApiException as e:
    print("Exception when calling InterfaceinfoApi->interfaceinfo_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this interface info. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **interfaceinfo_list**
> InlineResponse2008 interfaceinfo_list(page=page)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.InterfaceinfoApi(swagger_client.ApiClient(configuration))
page = 56 # int | A page number within the paginated result set. (optional)

try:
    api_response = api_instance.interfaceinfo_list(page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling InterfaceinfoApi->interfaceinfo_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| A page number within the paginated result set. | [optional] 

### Return type

[**InlineResponse2008**](InlineResponse2008.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **interfaceinfo_partial_update**
> InterfaceInfo interfaceinfo_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.InterfaceinfoApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this interface info.
data = swagger_client.InterfaceInfo() # InterfaceInfo | 

try:
    api_response = api_instance.interfaceinfo_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling InterfaceinfoApi->interfaceinfo_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this interface info. | 
 **data** | [**InterfaceInfo**](InterfaceInfo.md)|  | 

### Return type

[**InterfaceInfo**](InterfaceInfo.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **interfaceinfo_read**
> InterfaceInfo interfaceinfo_read(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.InterfaceinfoApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this interface info.

try:
    api_response = api_instance.interfaceinfo_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling InterfaceinfoApi->interfaceinfo_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this interface info. | 

### Return type

[**InterfaceInfo**](InterfaceInfo.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **interfaceinfo_update**
> InterfaceInfo interfaceinfo_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.InterfaceinfoApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this interface info.
data = swagger_client.InterfaceInfo() # InterfaceInfo | 

try:
    api_response = api_instance.interfaceinfo_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling InterfaceinfoApi->interfaceinfo_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this interface info. | 
 **data** | [**InterfaceInfo**](InterfaceInfo.md)|  | 

### Return type

[**InterfaceInfo**](InterfaceInfo.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

