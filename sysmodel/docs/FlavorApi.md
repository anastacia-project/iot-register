# swagger_client.FlavorApi

All URIs are relative to *http://195.148.125.100:9000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**flavor_create**](FlavorApi.md#flavor_create) | **POST** /flavor/ | 
[**flavor_delete**](FlavorApi.md#flavor_delete) | **DELETE** /flavor/{id}/ | 
[**flavor_list**](FlavorApi.md#flavor_list) | **GET** /flavor/ | 
[**flavor_partial_update**](FlavorApi.md#flavor_partial_update) | **PATCH** /flavor/{id}/ | 
[**flavor_read**](FlavorApi.md#flavor_read) | **GET** /flavor/{id}/ | 
[**flavor_update**](FlavorApi.md#flavor_update) | **PUT** /flavor/{id}/ | 


# **flavor_create**
> Flavor flavor_create(data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.FlavorApi(swagger_client.ApiClient(configuration))
data = swagger_client.Flavor() # Flavor | 

try:
    api_response = api_instance.flavor_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FlavorApi->flavor_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Flavor**](Flavor.md)|  | 

### Return type

[**Flavor**](Flavor.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **flavor_delete**
> flavor_delete(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.FlavorApi(swagger_client.ApiClient(configuration))
id = 'id_example' # str | A unique value identifying this flavor.

try:
    api_instance.flavor_delete(id)
except ApiException as e:
    print("Exception when calling FlavorApi->flavor_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| A unique value identifying this flavor. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **flavor_list**
> InlineResponse2006 flavor_list(page=page)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.FlavorApi(swagger_client.ApiClient(configuration))
page = 56 # int | A page number within the paginated result set. (optional)

try:
    api_response = api_instance.flavor_list(page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FlavorApi->flavor_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| A page number within the paginated result set. | [optional] 

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **flavor_partial_update**
> Flavor flavor_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.FlavorApi(swagger_client.ApiClient(configuration))
id = 'id_example' # str | A unique value identifying this flavor.
data = swagger_client.Flavor() # Flavor | 

try:
    api_response = api_instance.flavor_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FlavorApi->flavor_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| A unique value identifying this flavor. | 
 **data** | [**Flavor**](Flavor.md)|  | 

### Return type

[**Flavor**](Flavor.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **flavor_read**
> Flavor flavor_read(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.FlavorApi(swagger_client.ApiClient(configuration))
id = 'id_example' # str | A unique value identifying this flavor.

try:
    api_response = api_instance.flavor_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FlavorApi->flavor_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| A unique value identifying this flavor. | 

### Return type

[**Flavor**](Flavor.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **flavor_update**
> Flavor flavor_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.FlavorApi(swagger_client.ApiClient(configuration))
id = 'id_example' # str | A unique value identifying this flavor.
data = swagger_client.Flavor() # Flavor | 

try:
    api_response = api_instance.flavor_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FlavorApi->flavor_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| A unique value identifying this flavor. | 
 **data** | [**Flavor**](Flavor.md)|  | 

### Return type

[**Flavor**](Flavor.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

