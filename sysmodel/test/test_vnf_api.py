# coding: utf-8

"""
    Anastacia System Model

    Anastacia system model development version  # noqa: E501

    OpenAPI spec version: dev
    Contact: mohammed.boukhalfa@aato.fi
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.vnf_api import VnfApi  # noqa: E501
from swagger_client.rest import ApiException


class TestVnfApi(unittest.TestCase):
    """VnfApi unit test stubs"""

    def setUp(self):
        self.api = swagger_client.api.vnf_api.VnfApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_vnf_create(self):
        """Test case for vnf_create

        """
        pass

    def test_vnf_delete(self):
        """Test case for vnf_delete

        """
        pass

    def test_vnf_list(self):
        """Test case for vnf_list

        """
        pass

    def test_vnf_partial_update(self):
        """Test case for vnf_partial_update

        """
        pass

    def test_vnf_read(self):
        """Test case for vnf_read

        """
        pass

    def test_vnf_update(self):
        """Test case for vnf_update

        """
        pass


if __name__ == '__main__':
    unittest.main()
