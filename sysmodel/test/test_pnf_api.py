# coding: utf-8

"""
    Anastacia System Model

    Anastacia system model development version  # noqa: E501

    OpenAPI spec version: dev
    Contact: mohammed.boukhalfa@aato.fi
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.pnf_api import PNFApi  # noqa: E501
from swagger_client.rest import ApiException


class TestPNFApi(unittest.TestCase):
    """PNFApi unit test stubs"""

    def setUp(self):
        self.api = swagger_client.api.pnf_api.PNFApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_p_nf_create(self):
        """Test case for p_nf_create

        """
        pass

    def test_p_nf_delete(self):
        """Test case for p_nf_delete

        """
        pass

    def test_p_nf_list(self):
        """Test case for p_nf_list

        """
        pass

    def test_p_nf_partial_update(self):
        """Test case for p_nf_partial_update

        """
        pass

    def test_p_nf_read(self):
        """Test case for p_nf_read

        """
        pass

    def test_p_nf_update(self):
        """Test case for p_nf_update

        """
        pass


if __name__ == '__main__':
    unittest.main()
