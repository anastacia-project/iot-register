#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
IoT notifier implementation for ANASTACIA European Project. This module is in charge to format properly the messages to notify the IoT 
"""
import requests
import json
import logging
import uuid
import time
from datetime import datetime

#import os, sys
#parentPath = os.path.abspath("..")
#if parentPath not in sys.path:
#	sys.path.insert(0, parentPath)

import sysmodel
from sysmodel.models import *

from settings import remote_logger, SM_USER, SM_PASS, SECURITY_ORCHESTRATOR_API

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class IoTRegistrationNotifier(object):

	# We can not use iot_controller directly due to django disallowed header host
	#IOT_CONTROLLER_ADDR = "172.18.0.1"
	#SECURITY_ORCHESTRATOR_ADDR = "172.18.0.1"
	IOT_CONTROLLER_ADDR = "10.79.7.54"
	#SECURITY_ORCHESTRATOR_ADDR = "10.79.7.33"

	def notify(self, data):
		headers = {'Content-Type': 'application/{}'.format(self.type)}
		try:
			#response = requests.post(self.target, json=json_message)
			response = requests.post(self.target, data=data, headers=headers)
			logger.info("Response from notified entity:")
			logger.info(response)
		except Exception as e:
			logger.info(str(e))


class IoTControllerIoTRegistrationNotifier(IoTRegistrationNotifier):

	def __init__(self):
		self.target = "http://{}:8003/api/v1/register/".format(self.IOT_CONTROLLER_ADDR)

	def notify(self, json_message):
		# Just send the message
		super(IoTControllerIoTRegistrationNotifier,self).notify(json_message)
		#pass


class SystemModelIoTRegistrationNotifier(IoTRegistrationNotifier):

	def __init__(self):
		#self.target = "http://{}:8003/api/v1/register/".format(self.IOT_CONTROLLER_ADDR)
		pass

	def notify(self, json_message):
		# Just send the message
		pass
		#super(IoTControllerIoTRegistrationNotifier,self).notify(json_message)
		#pass


class SecurityOrchestratorIoTRegistrationNotifier(IoTRegistrationNotifier):

	def __init__(self):
		#self.target = "http://{}:8002/beservice/".format(self.SECURITY_ORCHESTRATOR_ADDR)
		#self.target = "http://{}:8002/meservice/".format(self.SECURITY_ORCHESTRATOR_ADDR)
		self.target = SECURITY_ORCHESTRATOR_API
		self.type = "xml"

	
	def notify(self, json_message):
		# Retrieve a FWD template from the Policy repository and fill it with the received information.
		POLICY_REPOSITORY_SERVICE_URL = "http://policy-repository.es:8004/api/v1/mspl-templates/?capabilities=divert"
		#POLICY_REPOSITORY_SERVICE_URL = "http://10.79.7.30:8004/api/v1/mspl-templates/?capability=divert"

		source = json_message["ifaces"][0]["ip_addr"]
		targets = json_message["authorization_info"]

		"""
		try:
			response = requests.get(POLICY_REPOSITORY_SERVICE_URL)
			json_response = json.loads(response.text)
			logger.info(json_response)
		except Exception as e:
			logger.info(str(e))
			return
		"""
		# The Security orchestrator must allow DTLS traffic between IoT device and IoT Broker
		# Load DTLS template for IoT devices
		f=open("bi_DTLS_template.xml", "r")
		dtls_template = f.read()
		# TODO: Loop over each destination
		#source = ":".join(source[i:i+4] for i in range(0,len(source),4))
		# Add the /128 for a ipv6 host
		source = source+"/128"
		target = targets[0]["de"]
		dtls_policy = dtls_template.replace("[DESTINATION-ADDRESS]",target).replace("[SOURCE-ADDRESS]",source)\
		.replace("[OMSPL_ID]","omspl_{}".format(uuid.uuid4().hex))\
		.replace("[MSPL_1_ID]","mspl_{}".format(uuid.uuid4().hex))\
		.replace('[MSPL_2_ID]',"mspl_{}".format(uuid.uuid4().hex))

		logger.info(dtls_policy)
		logging_message = {
		 "timestamp": datetime.timestamp(datetime.now()),
		 "from_module": "IOT_CONTROLLER",
		 "from_component": "IOT_REGISTER",
		 "to_module": "ORCHESTRATOR",
		 "to_component": "ORCHESTRATOR",
		 "incoming": False,
		 "method": "REST",
		 "data": dtls_policy,
		 "notes": "AuthZ enforcement"
		}
		remote_logger.info(logging_message)
		"""
		f= open("mspl-result-onauth.mspl","w+")
		f.write(dtls_policy)
		f.close()
		"""
		# The Security orchestrator must allow all traffic according on the new IoT device capabilties
		"""
		mspl_fwd_template = json_response[0]["mspl"]
		logger.info(mspl_fwd_template)
		mspl_list = []
		for target in targets:
			target = target["de"]
			mspl_fwd_1 = mspl_fwd_template.replace("<SourceAddress>","<SourceAddress>{}".format(source))\
			.replace("<DestinationAddress>","<DestinationAddress>{}".format(target))\
			.replace('ID=""','ID="{}"'.format("mspl_{}".format(uuid.uuid4().hex)))
			mspl_list.append({"mspl":mspl_fwd_1})
			mspl_fwd_2 = mspl_fwd_template.replace("<SourceAddress>","<SourceAddress>{}".format(target))\
			.replace("<DestinationAddress>","<DestinationAddress>{}".format(source))\
			.replace('ID=""','ID="{}"'.format("mspl_{}".format(uuid.uuid4().hex)))
			mspl_list.append({"mspl":mspl_fwd_2})
		json_response = {"mspl_list":mspl_list}
		logger.info(json.dumps(json_response))
		"""
		#super(SecurityOrchestratorIoTRegistrationNotifier,self).notify(json_response)
		super(SecurityOrchestratorIoTRegistrationNotifier,self).notify(dtls_policy)


class SystemModelIoTRegistrationNotifier(IoTRegistrationNotifier):

	def notify(self, json_message):
		self.register_obj = json_message
		self.register_iot_device()


	def basic_auth_api(self, api_instance):
		# Configure HTTP basic authorization: basicAuth
		api_instance.api_client.configuration.username = SM_USER
		api_instance.api_client.configuration.password = SM_PASS

	# Device
	def register_device(self,room,resources,software_list):
		print("softwares list: {}".format(software_list))
		tag = self.register_obj.get("tag",self.register_obj["name"]) 
		pnf = PNF(id=self.register_obj["id"],room=room.id,type="1",state="1",name=self.register_obj["name"],\
			tag=tag,model=self.register_obj["model"],\
			description=self.register_obj["name"],alias=self.register_obj["name"],\
			resources=resources,softwares=software_list)
		api_instance = sysmodel.PNFApi()
		self.basic_auth_api(api_instance)
		logger.debug(pnf)
		stored_device = api_instance.p_nf_create(pnf)
		logger.debug(stored_device)
		return stored_device
		

	def register_software(self):
		# Software (including operating system)
		# This only create a new software, but the relationship with the device is missing
		software_list = []
		for soft in self.register_obj["software"]:
			software = Software(name=soft["name"],version=soft["version"])
			api_instance = sysmodel.SoftwareApi()
			self.basic_auth_api(api_instance)
			stored_software = api_instance.software_create(software)
			logger.debug(stored_software)
			software_list.append(stored_software.id)
		return software_list


	def register_resources(self):
		# Resources
		resources = []
		for res in self.register_obj["resources"]:
			# Resource and Device link MISSING
			#logger.debug(res)
			resource = Resource(resource_name=res["description"],method=res["method"],url=res["resource"],port=res["port"])#,device=1)
			#logger.debug(resource)
			api_instance = sysmodel.ResourceApi()
			self.basic_auth_api(api_instance)
			stored_resource = api_instance.resource_create(resource)
			logger.debug(stored_resource)
			resources.append(stored_resource.id)
		return resources

	def register_ifaces(self, device):
		# Interfaces
		for iface in self.register_obj["ifaces"]:
			# Interface
			interface = DeviceInterface(type='1',name=iface["name"],device=device.id)
			logger.debug(interface)
			api_instance = sysmodel.DeviceinterfaceApi()
			self.basic_auth_api(api_instance)
			stored_interface = api_instance.deviceinterface_create(interface)
			logger.debug(stored_interface)

			# Interface info
			# Reformat the ipv6 in inface info
			#ip_addr = iface["ip_addr"]
			#iface["ip_addr"] = ":".join(ip_addr[i:i+4] for i in range(0,len(ip_addr),4))
			# Add the /128 for a ipv6 host
			iface["ip_addr"] = iface["ip_addr"]+"/128"
			interface_info = InterfaceInfo(mac=iface["mac_addr"],global_ipv6=iface["ip_addr"],interface=stored_interface.id)
			#interface.interfaceinfos = interface_info
			#logger.debug(interface)
			
			api_instance = sysmodel.InterfaceinfoApi()
			self.basic_auth_api(api_instance)
			api_response = api_instance.interfaceinfo_create(interface_info)
			logger.debug(api_response)

	def register_location(self):
		location = Location(latitude=self.register_obj["location"]["latitude"],longitude=self.register_obj["location"]["longitude"],\
			zone=self.register_obj["location"]["zone"],description=self.register_obj["location"]["description"])
		#api_instance = sysmodel.LocationApi()
		#self.basic_auth_api(api_instance)
		logger.debug(location)
		return location


	def get_device_by_name(self):
		logger.debug("GET DEVICE BY NAME {}".format(self.register_obj["name"]))
		api_instance = sysmodel.PNFApi()
		self.basic_auth_api(api_instance)
		devices = api_instance.p_nf_list(name=self.register_obj["name"])
		logger.debug(devices.results)
		return devices.results[0] if devices.results else None

	def register_room(self):

		# Get the room
		logger.debug("GET ROOM BY NAME {}".format(self.register_obj["location"]["description"]))
		api_instance = sysmodel.RoomApi()
		self.basic_auth_api(api_instance)
		# FILTER BY ROOM NAME IS MISSING
		rooms = api_instance.room_list(name=self.register_obj["location"]["description"])
		logger.debug(rooms.results)
		if rooms.results:
			return rooms.results[0]

		# otherwise we will create it
		room = sysmodel.Room(latitude=self.register_obj["location"]["latitude"],longitude=self.register_obj["location"]["longitude"],\
			zone=self.register_obj["location"]["zone"],name=self.register_obj["location"]["description"], \
			description=self.register_obj["location"]["description"])
		api_instance = sysmodel.RoomApi()
		self.basic_auth_api(api_instance)
		stored_room = api_instance.room_create(room)
		logger.debug(stored_room)

		return stored_room


	def register_room_interface(self, room):

		# Get the room interface
		logger.debug("GET ROOM INTERFACES FOR ROOM {}".format(room.id))
		api_instance = sysmodel.RoominterfaceApi()
		self.basic_auth_api(api_instance)
		room_interfaces = api_instance.roominterface_list(room=room.id)
		logger.debug(room_interfaces)
		if room_interfaces.results:
			return room_interfaces.results[0]

		# Static room interface
		ROOMS_INTERFACES = {"PEANA1":"of:000b1c98ec7f9b00/13",
							"PEANA2":"of:000b1c98ec7f9b00/9",}
		room_interface = sysmodel.RoomInterface(type='1',name=ROOMS_INTERFACES[room.name],room=room.id)
		logger.debug(room_interface)
		api_instance = sysmodel.RoominterfaceApi()
		self.basic_auth_api(api_instance)
		stored_interface = api_instance.roominterface_create(room_interface)
		logger.debug(stored_interface)
	
		return stored_interface


	def set_online2(self,device):
		if device:
			device.state = "2"
			api_instance = sysmodel.PNFApi()
			self.basic_auth_api(api_instance)
			device = api_instance.p_nf_update(id=device.id,data=device)
			print("DEVICE UPDATED")
			logger.debug(device)
		
	def register_iot_device(self):
		# If already exist, none
		logging_message = {
		 "timestamp": datetime.timestamp(datetime.now()),
		 "from_module": "IOT_CONTROLLER",
		 "from_component": "IOT_REGISTER",
		 "to_module": "ORCHESTRATOR",
		 "to_component": "ORCHESTRATOR",
		 "incoming": False,
		 "method": "REST",
		 "data": "System model update request by using system model client",
		 "notes": "System model update"
		}
		remote_logger.info(logging_message)
		device = self.get_device_by_name()
		logger.debug(device)
		if not device:
			resources = self.register_resources()
			software_list = self.register_software()
			#location = self.register_location()
			room = self.register_room()
			self.register_room_interface(room)
			device = self.register_device(room,resources,software_list)
			ifaces = self.register_ifaces(device)
			print("DEVICE CREATED")
		else:
			self.set_online2(device)
		
